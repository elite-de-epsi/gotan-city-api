import { Module } from '@nestjs/common';
import { GeojsonService } from './geojson.service';
import { GeojsonController } from './geojson.controller';
import { GeojsonRepository } from './geojson.repository';
import { CommonModule } from '../common/common.module';
import { DatabaseModule } from '../database/database.module';

@Module({
  imports: [CommonModule, DatabaseModule],
  controllers: [GeojsonController],
  providers: [GeojsonService, GeojsonRepository],
})
export class GeojsonModule {}
