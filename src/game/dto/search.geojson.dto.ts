export interface StopProperties {
  name: string;
  type: string;
  wheelchair_accessible: boolean;
  feature_type: 'stop';
}

export interface RouteProperties {
  short_name: string;
  long_name: string;
  color: string;
  picture: string;
  feature_type: 'route';
}

export type FeatureProperties = StopProperties | RouteProperties;

export interface GeojsonFeature {
  type: string;
  id: string;
  properties: FeatureProperties;
  geometry: {
    type: string;
    coordinates: number[];
  };
}

export interface SearchGeojsonDto {
  type: string;
  features: GeojsonFeature[];
}

export interface GeoJsonResult {
  geojson: SearchGeojsonDto;
}
