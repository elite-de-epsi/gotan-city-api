import { StopTypeEnum } from '../enum/stop.type.enum';

export class SearchGeojsonPayload {
  stopType?: StopTypeEnum;
  stopName?: string;
  getStops?: boolean = true;
  getRoutes?: boolean = false;
}
