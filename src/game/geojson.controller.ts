import { httpMethods } from '../common/http-method/http-method.constant';
import { Controller, Get, Query, Version } from '@nestjs/common';
import { SearchGeojsonDto } from './dto/search.geojson.dto';
import { GeojsonService } from './geojson.service';
import { SearchGeojsonPayload } from './payload/search.geojson.payload';
import {
  ApiHeader,
  ApiOperation,
  ApiResponse,
  ApiSecurity,
  ApiTags,
} from '@nestjs/swagger';
import { Observable } from 'rxjs';
import { apiKeyHeaderName } from '../common/api-key/service/api-key.service';

@Controller('/api/geojson')
@ApiTags('geojson')
@ApiSecurity(apiKeyHeaderName)
@ApiHeader({ name: 'version', required: true, description: 'API version' })
export class GeojsonController {
  constructor(private readonly geojsonService: GeojsonService) {}

  @Get('')
  @Version('1')
  @ApiOperation({
    summary: 'Get geojson',
    description: 'This API call permit to get geojson with different filter.',
  })
  @ApiResponse({
    status: httpMethods.GET.statusCode,
    description: 'It should a return a Geojson.',
  })
  getGeojson(
    @Query() payload: SearchGeojsonPayload,
  ): Observable<SearchGeojsonDto> {
    return this.geojsonService.getGeojson(payload);
  }
}
