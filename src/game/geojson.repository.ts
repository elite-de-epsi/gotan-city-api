import { Injectable } from '@nestjs/common';
import { SearchGeojsonPayload } from './payload/search.geojson.payload';
import { DatabaseService } from '../database/database.service';
import { Observable } from 'rxjs';
import { Query } from 'pg';
import { GeoJsonResult } from './dto/search.geojson.dto';

@Injectable()
export class GeojsonRepository {
  constructor(private databaseService: DatabaseService) {}

  /**
   * Search geojson
   * @param payload
   * @returns {Observable<GeoJsonResult>}
   * @since 0.0.1
   * @version 0.0.1
   * @name searchGeojson
   * @description
   * This request permit to search stops and routes with different filter and return a geojson.
   */
  searchGeojson(payload: SearchGeojsonPayload): Observable<GeoJsonResult> {
    const query: Query = {
      name: 'search-geojson',
      text: `
        SELECT public.get_stops_and_routes_geojson($1, $2, $3, $4) as geojson;
      `,
      values: [
        payload.stopType,
        payload.stopName,
        payload.getStops,
        payload.getRoutes,
      ],
    };

    return this.databaseService.one<GeoJsonResult>(query);
  }
}
