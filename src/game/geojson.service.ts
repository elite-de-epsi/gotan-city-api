import { Injectable } from '@nestjs/common';
import { GeojsonRepository } from './geojson.repository';
import { GeoJsonResult, SearchGeojsonDto } from './dto/search.geojson.dto';
import { Observable } from 'rxjs';
import { SearchGeojsonPayload } from './payload/search.geojson.payload';
import { map } from 'rxjs/operators';

@Injectable()
export class GeojsonService {
  constructor(private geojsonRepository: GeojsonRepository) {}

  /**
   * Search geojson
   * @param payload {SearchGeojsonPayload} The payload to search games
   * @returns {Promise<SearchGeojsonDto>} The list of games
   * @description
   * This API call permit to search stops and routes with different filter and return a geojson.
   */
  getGeojson(payload: SearchGeojsonPayload): Observable<SearchGeojsonDto> {
    return this.geojsonRepository.searchGeojson(payload).pipe(
      map((geojsonResult: GeoJsonResult) => {
        return geojsonResult.geojson;
      }),
    );
  }
}
