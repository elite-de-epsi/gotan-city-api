import { CheckFormatService } from './check-format.service';
import { Test, TestingModule } from '@nestjs/testing';
import { FunctionalException } from '../exception/functional.exception';

describe('CheckFormatService', () => {
  let service: CheckFormatService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [CheckFormatService],
    }).compile();

    service = module.get<CheckFormatService>(CheckFormatService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('checkUuidFormat', () => {
    it('should throw an error if the uuid is not a valid uuid format', (done) => {
      expect.assertions(3);

      service.checkUuidFormat('bad-uuid-format').subscribe({
        error: (error) => {
          expect(error).toBeInstanceOf(FunctionalException);
          expect(error.status).toBe(400);
          expect(error.error).toBe('bad_request');
          done();
        },
      });
    });

    it('should not throw an error if the uuid is a valid uuid format', () => {
      const uuid = '1701a408-df91-43b3-a965-99b15a83c040';
      service.checkUuidFormat(uuid).subscribe((result) => {
        expect(result).toBe(true);
      });
    });
  });
});
