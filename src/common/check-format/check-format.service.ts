import { FunctionalException } from '../exception/functional.exception';
import { HttpStatus, Injectable } from '@nestjs/common';
import { Observable, of, throwError } from 'rxjs';

@Injectable()
export class CheckFormatService {
  checkUuidFormat(uuid: string): Observable<boolean> {
    let result$ = of(true);

    if (
      !uuid.match(
        /^[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[1-5][0-9a-fA-F]{3}-[89abAB][0-9a-fA-F]{3}-[0-9a-fA-F]{12}$/,
      )
    ) {
      result$ = throwError(
        () =>
          new FunctionalException(
            HttpStatus.BAD_REQUEST,
            'bad_request',
            'The id must be a valid uuid format',
            'Try to provide a well formatted id',
            'https://en.wikipedia.org/wiki/Universally_unique_identifier',
          ),
      );
    }

    return result$;
  }
}
