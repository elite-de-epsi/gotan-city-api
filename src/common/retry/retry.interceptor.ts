import {
  CallHandler,
  ExecutionContext,
  Inject,
  Injectable,
  NestInterceptor,
} from '@nestjs/common';
import { Observable, retry, throwError } from 'rxjs';
import { catchError, delay } from 'rxjs/operators';

@Injectable()
export class RetryInterceptor implements NestInterceptor {
  constructor(
    @Inject('RETRY') private readonly retry: number,
    @Inject('DELAY') private readonly delay: number,
  ) {}

  intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
    return next.handle().pipe(
      retry(this.retry),
      delay(this.delay),
      catchError((error) => {
        return throwError(() => error);
      }),
    );
  }
}
