import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { apiKeyHeaderName, ApiKeyService } from './service/api-key.service';

@Injectable()
export class ApiKeyGuard implements CanActivate {
  constructor(private apiKeyService: ApiKeyService) {}

  canActivate(context: ExecutionContext): boolean {
    const req = context.switchToHttp().getRequest();
    const apiKey: string = req.headers[apiKeyHeaderName];

    // Check if the api key is provided
    this.apiKeyService.throwErrorIfKeyNotExist(apiKey);

    // Check if the api key is correct
    this.apiKeyService.throwErrorIfKeyIsIncorrect(apiKey);

    return true;
  }
}
