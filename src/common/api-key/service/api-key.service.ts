import { HttpStatus, Injectable } from '@nestjs/common';
import { FunctionalException } from '../../exception/functional.exception';

export const apiKeyHeaderName = 'x-api-key';

@Injectable()
export class ApiKeyService {
  throwErrorIfKeyNotExist(apiKey): void {
    if (!apiKey) {
      throw new FunctionalException(
        HttpStatus.UNAUTHORIZED,
        'missing_api_key',
        'You must provide an API key.',
        'Try to provide a good API key. You cant access to the API without it.',
        'https://driing.app/how-to-access-api',
      );
    }
  }

  throwErrorIfKeyIsIncorrect(apiKey: string): void {
    const isTestEnvironment = process.env.NODE_ENV === 'test';
    const isE2ETestApiKey = apiKey === process.env.API_KEY_E2E_TEST;
    const isFrontendApiKey = apiKey === process.env.API_KEY_DRIING_FRONTEND;
    const validApiKey =
      (isTestEnvironment && isE2ETestApiKey) || isFrontendApiKey;

    if (!validApiKey) {
      throw new FunctionalException(
        HttpStatus.UNAUTHORIZED,
        'invalid_api_key',
        'The api key you provided is incorrect',
        'Try to provide a good api key.',
        'https://driing.app/how-to-access-api',
      );
    }
  }
}
