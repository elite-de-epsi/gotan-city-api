import { Test, TestingModule } from '@nestjs/testing';
import { FunctionalException } from '../../exception/functional.exception';
import { ApiKeyService } from './api-key.service';

const e2eApiKey = 'e2e_api_key';
const frontendApiKey = 'frontend_api_key';
const incorrectApiKey = 'incorrect_api_key';
describe('ApiKeyService', () => {
  let service: ApiKeyService;

  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ApiKeyService],
    }).compile();

    service = module.get<ApiKeyService>(ApiKeyService);

    process.env.API_KEY_E2E_TEST = e2eApiKey;
    process.env.API_KEY_DRIING_FRONTEND = frontendApiKey;
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('throwErrorIfKeyIsIncorrect', () => {
    it('should throw an error if the api key is incorrect on test environment', () => {
      process.env.NODE_ENV = 'test';

      expect(() =>
        service.throwErrorIfKeyIsIncorrect(incorrectApiKey),
      ).toThrowError(FunctionalException);
    });

    it('should not throw an error if the api key is correct on test environment', () => {
      process.env.NODE_ENV = 'test';

      expect(() =>
        service.throwErrorIfKeyIsIncorrect(e2eApiKey),
      ).not.toThrowError(FunctionalException);
    });

    it('should throw an error if the api key is incorrect on production environment', () => {
      process.env.NODE_ENV = 'production';

      expect(() =>
        service.throwErrorIfKeyIsIncorrect(incorrectApiKey),
      ).toThrowError(FunctionalException);
    });

    it('should not throw an error if the api key is correct on production environment', () => {
      process.env.NODE_ENV = 'production';

      expect(() =>
        service.throwErrorIfKeyIsIncorrect(frontendApiKey),
      ).not.toThrowError(FunctionalException);
    });
  });
});
