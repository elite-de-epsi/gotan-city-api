export class PaginationPayload {
  keyword?: string;

  size?: number = 10;

  page?: number = 0;
}
