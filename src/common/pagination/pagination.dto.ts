export interface PaginationDto<T> {
  totalPageAmount: number;
  totalItemsAmount: number;
  currentPageNumber: number;
  itemsAmountPerPage: number;
  data: T[];
}
