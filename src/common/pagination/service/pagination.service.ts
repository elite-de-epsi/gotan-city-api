import { Injectable } from '@nestjs/common';
import { PaginationPayload } from '../pagination.payload';

@Injectable()
export class PaginationService {
  public listViewToPaginationDto<T>(paginationInfo: PaginationPayload) {
    return (listView) => {
      let itemsAmount = 0;
      let pageAmount = 0;
      if (listView.length > 0) {
        itemsAmount = listView[0].totalItemsAmount;
        pageAmount = Math.ceil(itemsAmount / paginationInfo.size);
      }
      return {
        currentPageNumber: +paginationInfo.page,
        itemsAmountPerPage: +paginationInfo.size,
        totalPageAmount: pageAmount,
        totalItemsAmount: itemsAmount,
        data: listView.map((view) => {
          const dto = {
            ...view,
          };

          delete dto.totalItemsAmount;

          return dto as T;
        }),
      };
    };
  }
}
