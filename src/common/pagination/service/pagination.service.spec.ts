import { PaginationService } from './pagination.service';
import { PaginationPayload } from '../pagination.payload';

describe('PaginationService', () => {
  let paginationService: PaginationService;

  beforeEach(() => {
    paginationService = new PaginationService();
  });

  describe('listViewToPaginationDto', () => {
    it('should return the correct pagination DTO', () => {
      const paginationInfo: PaginationPayload = { page: 1, size: 10 };
      const listView = [
        { id: 1, name: 'Item 1', totalItemsAmount: 20 },
        { id: 2, name: 'Item 2', totalItemsAmount: 20 },
      ];

      const expectedPaginationDto = {
        currentPageNumber: 1,
        itemsAmountPerPage: 10,
        totalPageAmount: 2,
        totalItemsAmount: 20,
        data: [
          { id: 1, name: 'Item 1' },
          { id: 2, name: 'Item 2' },
        ],
      };

      const listViewToPaginationDto =
        paginationService.listViewToPaginationDto<any>(paginationInfo);

      const actualPaginationDto = listViewToPaginationDto(listView);

      expect(actualPaginationDto).toEqual(expectedPaginationDto);
    });
  });
});
