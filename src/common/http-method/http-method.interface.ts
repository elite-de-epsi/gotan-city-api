export interface HttpPatchResponse {
  resourceId: string;
  nbLinesUpdated: number;
}
