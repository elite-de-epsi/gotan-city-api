export type HttpMethodResponse = {
  name: string;
  statusCode: number;
  isReading: boolean;
};

export type HttpMethodResponseList = {
  GET: HttpMethodResponse;
  POST: HttpMethodResponse;
  PUT: HttpMethodResponse;
  PATCH: HttpMethodResponse;
  DELETE: HttpMethodResponse;
};

export const httpMethods: HttpMethodResponseList = {
  GET: {
    name: 'retrieved',
    statusCode: 200,
    isReading: true,
  },
  POST: {
    name: 'created',
    statusCode: 201,
    isReading: false,
  },
  PUT: {
    name: 'updated',
    statusCode: 200,
    isReading: false,
  },
  PATCH: {
    name: 'updated',
    statusCode: 200,
    isReading: false,
  },
  DELETE: {
    name: 'deleted',
    statusCode: 204,
    isReading: false,
  },
};
