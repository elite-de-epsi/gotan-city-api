import {
  Injectable,
  NestInterceptor,
  ExecutionContext,
  CallHandler,
} from '@nestjs/common';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { HttpMethodResponse, httpMethods } from '../http-method.constant';
import { HttpPatchResponse } from '../http-method.interface';

@Injectable()
export class HttpMethodInterceptor implements NestInterceptor {
  intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
    let handle = next.handle();
    // If method is in actionMethods, then we return a custom response
    const method: HttpMethodResponse = httpMethods[context.getArgs()[0].method];
    if (method && !method.isReading) {
      handle = handle.pipe(
        map((data: HttpPatchResponse) => ({
          statusCode: method.statusCode,
          message: `Resource ${method.name} successfully`,
          resourceId: data.resourceId,
          nbLinesUpdated: data.nbLinesUpdated,
        })),
      );
    }

    return handle;
  }
}
