import { HttpException } from '@nestjs/common';

export class TechnicalException extends HttpException {
  constructor(message: string) {
    const error = 'internal_server_error';

    super(error, 500);
    this.error = error;
    this.message = message;

    this.fix = 'Retry later.';
    this.moreInfoUrl = 'https://more_info_url/errors/internal_server_error';
  }
  error: string;
  message: string;

  fix?: string;
  moreInfoUrl?: string;
}
