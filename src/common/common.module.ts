import { Module } from '@nestjs/common';
import { PaginationService } from './pagination/service/pagination.service';
import { CheckFormatService } from './check-format/check-format.service';
import { LoggerService } from './logger/logger.service';
import { ApiKeyService } from './api-key/service/api-key.service';

@Module({
  providers: [
    PaginationService,
    CheckFormatService,
    LoggerService,
    ApiKeyService,
  ],
  exports: [
    PaginationService,
    CheckFormatService,
    LoggerService,
    ApiKeyService,
  ],
})
export class CommonModule {}
