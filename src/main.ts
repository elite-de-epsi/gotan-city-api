import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import helmet from 'helmet';
import { INestApplication, VersioningType } from '@nestjs/common';
import { HttpExceptionFilter } from './common/exception/filter/http.exception.filter';
import { TimeoutInterceptor } from './common/timeout/timeout.interceptor';
import { RetryInterceptor } from './common/retry/retry.interceptor';
import { LoggerService } from './common/logger/logger.service';
import { HttpMethodInterceptor } from './common/http-method/interceptor/http-method.interceptor';

function setupSwagger(app: INestApplication) {
  const config = new DocumentBuilder()
    .setTitle('Gotan city API')
    .setDescription('This the swagger documentation of the Gotan city API !')
    .setVersion('0.1')
    .addApiKey({ type: 'apiKey', name: 'x-api-key', in: 'header' }, 'x-api-key')
    .build();
  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('api/doc', app, document);
}

function setupHeaders(app: INestApplication) {
  // Set up cors headers
  app.enableCors({
    origin: '*',
    methods: 'GET,HEAD,PUT,PATCH,POST,DELETE',
    allowedHeaders: ['Content-Type', 'Authorization'],
  });
  // Set the Content-Security-Policy header
  app.use(
    helmet.contentSecurityPolicy({
      directives: {
        defaultSrc: ["'self'"],
        baseUri: ["'self'"],
        fontSrc: ["'self'", 'https:', 'data:'],
        formAction: ["'self'"],
        frameAncestors: ["'self'"],
        imgSrc: ["'self'", 'data:'],
        objectSrc: ["'none'"],
        scriptSrc: ["'self'"],
        scriptSrcAttr: ["'none'"],
        styleSrc: ["'self'", 'https:', "'unsafe-inline'"],
        upgradeInsecureRequests: [],
      },
    }),
  );

  // Set the X-Content-Type-Options header
  app.use(helmet.noSniff());

  // Set the X-Download-Options header
  app.use(helmet.ieNoOpen());

  // Set the X-Frame-Options header
  app.use(helmet.frameguard({ action: 'sameorigin' }));

  // Set the X-Permitted-Cross-Domain-Policies header
  app.use(helmet.permittedCrossDomainPolicies());

  // Set the Strict-Transport-Security header
  app.use(
    helmet.hsts({
      maxAge: 15552000,
      includeSubDomains: true,
    }),
  );

  // Set the Referrer-Policy header
  app.use(helmet.referrerPolicy({ policy: 'no-referrer' }));

  // Set the X-XSS-Protection header
  app.use(helmet.xssFilter());
}

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  setupSwagger(app);

  setupHeaders(app);

  app.enableVersioning({
    type: VersioningType.HEADER,
    header: 'version',
  });

  const loggerService = new LoggerService();
  app.useGlobalFilters(new HttpExceptionFilter(loggerService));

  app.useGlobalInterceptors(new TimeoutInterceptor(100000));
  app.useGlobalInterceptors(new RetryInterceptor(2, 1000));
  app.useGlobalInterceptors(new HttpMethodInterceptor());

  await app.listen(3005, '0.0.0.0');
}
bootstrap();
