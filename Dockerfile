FROM node:14-alpine

# Ce place dans le répertoire de travail
WORKDIR /usr/src/app

# Ajout du dossier courant dans le répertoire de travail
COPY . .

# Vérifie que le répertoire dist existe bien
RUN test -d dist || { echo "Le répertoire dist n'existe pas !"; exit 1; }

# Lance l'application
CMD ["node", "dist/main"]