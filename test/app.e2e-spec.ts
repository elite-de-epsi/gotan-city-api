import { Test, TestingModule } from '@nestjs/testing';
import { HttpStatus, INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import * as process from 'process';
import { testApiKey } from '../src/common/api-key/api-key.mock';
import { AppModule } from '../src/app.module';
import { LoggerService } from '../src/common/logger/logger.service';
import { HttpExceptionFilter } from '../src/common/exception/filter/http.exception.filter';
import { TimeoutInterceptor } from '../src/common/timeout/timeout.interceptor';
import { RetryInterceptor } from '../src/common/retry/retry.interceptor';
import { HttpMethodInterceptor } from '../src/common/http-method/interceptor/http-method.interceptor';

describe('GameController (e2e)', () => {
  let app: INestApplication;

  beforeAll(async () => {
    process.env.NODE_ENV = 'test';
    process.env.API_KEY_E2E_TEST = testApiKey;

    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();

    const loggerService = new LoggerService();
    app.useGlobalFilters(new HttpExceptionFilter(loggerService));

    app.useGlobalInterceptors(new TimeoutInterceptor(3000));
    app.useGlobalInterceptors(new RetryInterceptor(2, 1000));
    app.useGlobalInterceptors(new HttpMethodInterceptor());
  });

  afterAll(async () => {
    await app.close();
  });

  it('/games (GET)', () => {
    let result;
    try {
      result = request(app.getHttpServer())
        .get('/api/games')
        .set('x-api-key', 'the_incorrect_api_key')
        .set('version', '1')
        .expect(HttpStatus.UNAUTHORIZED);
    } catch (e) {
      console.log(e);
    }
    return result;
  });

  it('/games (GET)', () => {
    let result;
    try {
      result = request(app.getHttpServer())
        .get('/api/games')
        .set('x-api-key', testApiKey)
        .set('version', '1')
        .catch((e) => {
          console.log(e);
        });
    } catch (e) {
      console.log(e);
    }
    return result;
  });
});
