<p align="center">
  <a href="http://nestjs.com/" target="blank"><img src="https://nestjs.com/img/logo-small.svg" width="200" alt="Nest Logo" /></a>
</p>

[circleci-image]: https://img.shields.io/circleci/build/github/nestjs/nest/master?token=abc123def456
[circleci-url]: https://circleci.com/gh/nestjs/nest

  <p align="center">A progressive <a href="http://nodejs.org" target="_blank">Node.js</a> framework for building efficient and scalable server-side applications.</p>
    <p align="center">
<a href="https://www.npmjs.com/~nestjscore" target="_blank"><img src="https://img.shields.io/npm/v/@nestjs/core.svg" alt="NPM Version" /></a>
<a href="https://www.npmjs.com/~nestjscore" target="_blank"><img src="https://img.shields.io/npm/l/@nestjs/core.svg" alt="Package License" /></a>
<a href="https://www.npmjs.com/~nestjscore" target="_blank"><img src="https://img.shields.io/npm/dm/@nestjs/common.svg" alt="NPM Downloads" /></a>
<a href="https://circleci.com/gh/nestjs/nest" target="_blank"><img src="https://img.shields.io/circleci/build/github/nestjs/nest/master" alt="CircleCI" /></a>
<a href="https://coveralls.io/github/nestjs/nest?branch=master" target="_blank"><img src="https://coveralls.io/repos/github/nestjs/nest/badge.svg?branch=master#9" alt="Coverage" /></a>
<a href="https://discord.gg/G7Qnnhy" target="_blank"><img src="https://img.shields.io/badge/discord-online-brightgreen.svg" alt="Discord"/></a>
<a href="https://opencollective.com/nest#backer" target="_blank"><img src="https://opencollective.com/nest/backers/badge.svg" alt="Backers on Open Collective" /></a>
<a href="https://opencollective.com/nest#sponsor" target="_blank"><img src="https://opencollective.com/nest/sponsors/badge.svg" alt="Sponsors on Open Collective" /></a>
  <a href="https://paypal.me/kamilmysliwiec" target="_blank"><img src="https://img.shields.io/badge/Donate-PayPal-ff3f59.svg"/></a>
    <a href="https://opencollective.com/nest#sponsor"  target="_blank"><img src="https://img.shields.io/badge/Support%20us-Open%20Collective-41B883.svg" alt="Support us"></a>
  <a href="https://twitter.com/nestframework" target="_blank"><img src="https://img.shields.io/twitter/follow/nestframework.svg?style=social&label=Follow"></a>
</p>
  <!--[![Backers on Open Collective](https://opencollective.com/nest/backers/badge.svg)](https://opencollective.com/nest#backer)
  [![Sponsors on Open Collective](https://opencollective.com/nest/sponsors/badge.svg)](https://opencollective.com/nest#sponsor)-->

## Description

[Nest](https://github.com/nestjs/nest) framework TypeScript starter repository.

## Installation

```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```

## Documentation

/api/doc => Swagger. 

Useful links: https://stackoverflow.com/questions/60114023/how-to-add-summary-and-body-manually-in-swagger-nestjs.

## Start the database 
### 1. Créer des tables temporaires

```sql
    -- Create temporary tables
CREATE TABLE temp_stops (
    stop_id VARCHAR(100),
    stop_name VARCHAR(100),
    stop_desc VARCHAR(100),
    stop_lat DOUBLE PRECISION,
    stop_lon DOUBLE PRECISION,
    zone_id VARCHAR(100),
    stop_url VARCHAR(100),
    location_type INTEGER,
    parent_station VARCHAR(100),
    wheelchair_boarding INTEGER
);

CREATE TABLE temp_stop_times (
    trip_id VARCHAR(100),
    arrival_time VARCHAR(100),
    departure_time VARCHAR(100),
    stop_id VARCHAR(100),
    stop_sequence INTEGER,
    pickup_type INTEGER,
    drop_off_type INTEGER,
    timepoint INTEGER,
    stop_headsign VARCHAR(100)
);

CREATE TABLE temp_trips (
    route_id VARCHAR(100),
    service_id VARCHAR(100),
    trip_id VARCHAR(100),
    trip_headsign VARCHAR(100),
    direction_id INTEGER,
    block_id INTEGER,
    shape_id VARCHAR(100),
    wheelchair_accessible INTEGER
);

CREATE TABLE temp_shapes (
    shape_id VARCHAR(100),
    shape_pt_lat DOUBLE PRECISION,
    shape_pt_lon DOUBLE PRECISION,
    shape_pt_sequence INTEGER
);

CREATE TABLE temp_routes (
    route_id VARCHAR(100),
    route_short_name VARCHAR(50),
    route_long_name TEXT,
    route_desc TEXT,
    route_type INTEGER,
    route_color VARCHAR(6),
    route_text_color VARCHAR(6),
    route_sort_order INTEGER
);
```
### 2. Copier les données dans les tables temporaires 
Les commandes psql pour copier les données dans les tables temporaires. Il faut aller dans le terminal sur C:\Program Files\PostgreSQL\15\bin pour récupérer la commande et les lancer une par une.
```bash
    psql -h localhost -p 5432 -d gotan-city -U postgres -c "\copy temp_trips FROM 'D:\WORK\EPSI\I1\Data visualisation\gtfs-tan\trips.txt' DELIMITER ',' CSV HEADER;"
```
```bash
    psql -h localhost -p 5432 -d gotan-city -U postgres -c "\copy temp_routes FROM 'D:\WORK\EPSI\I1\Data visualisation\gtfs-tan\routes.txt' DELIMITER ',' CSV HEADER;"
```
```bash
    psql -h localhost -p 5432 -d gotan-city -U postgres -c "\copy temp_shapes FROM 'D:\WORK\EPSI\I1\Data visualisation\gtfs-tan\shapes.txt' DELIMITER ',' CSV HEADER;"
```
```bash
    psql -h localhost -p 5432 -d gotan-city -U postgres -c "\copy temp_stop_times FROM 'D:\WORK\EPSI\I1\Data visualisation\gtfs-tan\stop_times.txt' DELIMITER ',' CSV HEADER;"
```
```bash
    psql -h localhost -p 5432 -d gotan-city -U postgres -c "\copy temp_stops FROM 'D:\WORK\EPSI\I1\Data visualisation\gtfs-tan\stops.txt' DELIMITER ',' CSV HEADER;"
```

### 3. Créer les tables définitives
```bash
    db-migrate up
```
### 4. Utiliser les fonctions 
````sql 
SELECT public.get_stops_geojson();
SELECT public.get_routes_geojson();
````

## Support

Nest is an MIT-licensed open source project. It can grow thanks to the sponsors and support by the amazing backers. If you'd like to join them, please [read more here](https://docs.nestjs.com/support).

## Stay in touch

- Author - [Kamil Myśliwiec](https://kamilmysliwiec.com)
- Website - [https://nestjs.com](https://nestjs.com/)
- Twitter - [@nestframework](https://twitter.com/nestframework)

## License

Nest is [MIT licensed](LICENSE).
