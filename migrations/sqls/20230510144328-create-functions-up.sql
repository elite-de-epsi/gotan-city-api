CREATE OR REPLACE FUNCTION public.get_stops_and_routes_geojson(
    _stopType transport_type DEFAULT NULL,
    _stopName TEXT DEFAULT NULL,
    _getStops BOOLEAN DEFAULT TRUE,
    _getRoutes BOOLEAN DEFAULT TRUE
)
    RETURNS jsonb AS $$
BEGIN
    RETURN (
        SELECT json_build_object(
                       'type', 'FeatureCollection',
                       'features', json_agg(feature)
                   )
        FROM (
                 SELECT json_build_object(
                                'type', 'Feature',
                                'id', id,
                                'properties', json_build_object(
                                        'name', name,
                                        'type', type,
                                        'wheelchair_accessible', wheelchair_accessible,
                                        'feature_type', 'stop'
                                    ),
                                'geometry', ST_AsGeoJSON(coordinates)::json
                            ) AS feature
                 FROM public.stops
                 WHERE (_getStops = true AND ((_stopType IS NULL OR type = _stopType) AND (_stopName IS NULL OR name ILIKE '%' || _stopName || '%')))

                 UNION ALL

                 SELECT json_build_object(
                                'type', 'Feature',
                                'id', id,
                                'properties', json_build_object(
                                        'short_name', short_name,
                                        'long_name', long_name,
                                        'color', color,
                                        'picture', picture,
                                        'feature_type', 'route'
                                    ),
                                'geometry', ST_AsGeoJSON(coordinates)::json
                            ) AS feature
                 FROM (select * from public.routes order by "order" limit 6) as "ordered_routes"
                 WHERE (_getRoutes = true)
             ) AS subquery
    );
END;
$$ LANGUAGE plpgsql STABLE;